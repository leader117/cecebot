#!/bin/sh

sudo apt install -y lsb-release # make sure lsb-release is installed
wget https://raw.githubusercontent.com/dokku/dokku/v0.14.6/bootstrap.sh # fetch dokku bootstrap script
sudo DOKKU_TAG=v0.14.6 bash bootstrap.sh # install dokku

dokku apps:create botpress-server # create dokku app for botpress

sudo dokku plugin:install https://github.com/dokku/dokku-postgres.git # install PostgreSQL

dokku postgres:create botpressdb # create and link PostgreSQL db
dokku postgres:link botpressdb botpress-server

dokku --app botpress-server config:set DATABASE=postgres # make botpress use PostgreSQL
