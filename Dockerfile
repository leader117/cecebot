FROM botpress/server:v11_5_0
ADD data/ /botpress/data
WORKDIR /botpress
CMD ["./bp"]
